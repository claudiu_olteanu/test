#include "RN.hpp"
#include <sys/types.h>
#include <sys/epoll.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define ERR(fmt, ...) \
    do { \
        fprintf(stderr, "%s: " fmt, __PRETTY_FUNCTION__ , ##__VA_ARGS__); \
    } while (0)

RN::RN(int maxfds, int maxevents)
{
    m_epfd = epoll_create(maxfds);
    if (m_epfd < 1) {
        ERR("Error opening epoll fd: %s\n", strerror(errno));
        exit(1);
    }

    m_fds_alloc = 16;
    m_fds = (rn_client_t *)malloc(m_fds_alloc * sizeof(rn_client_t));
    if (!m_fds) {
        ERR("Cannot allocate fd array\n");
        exit(1);
    }

    m_maxevents = maxevents;
    m_events = (struct epoll_event *)malloc(m_maxevents * sizeof(struct epoll_event));
    if (!m_events) {
        ERR("Cannot allocate event array\n");
        exit(1);
    }

    pthread_mutex_init(&m_lock, NULL);
}

RN::~RN()
{
    int i;

    for (i = 0; i < m_fds_alloc; i++)
        if (m_fds[i].pfn)
            Remove(i);

    free(m_events);
    free(m_fds);
    close(m_epfd);
}

void RN::PrepareFD(int fd)
{
    int flags;

    flags = fcntl(fd, F_GETFL, 0);
    if(flags < 0) {
        ERR("fcntl(fd %d, F_GETFL, 0) err: %s\n", fd, strerror(errno));
        exit(1);
    }
    flags |= O_NONBLOCK;
    if (fcntl(fd, F_SETFL, flags) < 0) {
        ERR("fcntl(fd %d, F_SETFL, 0x%x) err: %s\n", fd, flags, strerror(errno));
        exit(1);
    }
}

int RN::AddIOCallback(int fd, rn_callback_fn_t pfn, void *data)
{
    int ret;
    pthread_mutex_lock(&m_lock);
    ret = Add(fd, pfn, data);
    pthread_mutex_unlock(&m_lock);
    return ret;
}

int RN::RemoveIOCallback(int fd)
{
    int ret;
    pthread_mutex_lock(&m_lock);
    ret = Remove(fd);
    pthread_mutex_unlock(&m_lock);
    return ret;
}

int RN::Add(int fd, rn_callback_fn_t pfn, void *data)
{
    struct epoll_event efd;
    int err;

    if (fd < 0) {
        ERR("Invalid fd %i\n", fd);
        return -EINVAL;
    }

    if (fd >= m_fds_alloc) {
        rn_client_t *newfds;
        int newsize;

        newsize = 2 * m_fds_alloc;
        if (newsize <= fd)
            newsize = fd + 1;

        newfds = (rn_client_t *)realloc(m_fds, newsize * sizeof(rn_client_t));
        if (!newfds) {
            ERR("Cannot resize fd array\n");
            return -ENOMEM;
        }
        memset(newfds + m_fds_alloc, 0, (newsize - m_fds_alloc) * sizeof(rn_client_t));

        m_fds = newfds;
        m_fds_alloc = newsize;
    }

    efd.events = EPOLLIN | EPOLLOUT | EPOLLET;
    efd.data.u64 = MAKE_CONTEXT(fd, m_fds[fd].fd_gen);
    err = epoll_ctl(m_epfd, EPOLL_CTL_ADD, fd, &efd);
    if (err < 0) {
        if (err != -EEXIST || m_fds[fd].pfn != pfn || m_fds[fd].data != data)
            ERR("Cannot add fd %i to the set, err %s\n", fd, strerror(errno));
        return -errno;
    }

    m_fds[fd].pfn = pfn;
    m_fds[fd].data = data;

    return 0;
}

int RN::Remove(int fd)
{
    struct epoll_event efd;
    int err = 0;

    if (fd < 0 || fd >= m_fds_alloc) {
        ERR("fd %i out of range\n", fd);
        return -EINVAL;
    }

    if (epoll_ctl(m_epfd, EPOLL_CTL_DEL, fd, &efd) < 0) {
        ERR("Error removing fd %i from the set: %s\n", fd, strerror(errno));
        err = -errno;
    }

    m_fds[fd].pfn = NULL;
    m_fds[fd].data = NULL;
    m_fds[fd].fd_gen++;

    return err;
}

bool RN::Exists(int fd)
{
    return fd > 0 && fd < m_fds_alloc && m_fds[fd].pfn != NULL;
}

int RN::WaitAndDispatchEvents(int timeout)
{
    int nfds, i;
    rn_pollevent_t event;
    int err = -EAGAIN;

    nfds = epoll_wait(m_epfd, m_events, m_maxevents, timeout);

    pthread_mutex_lock(&m_lock);
    for (i = 0; i < nfds; i++) {
        int fd = CONTEXT_FD(m_events[i].data.u64);
        unsigned fd_gen = CONTEXT_GEN(m_events[i].data.u64);

        if (fd < 0 || fd >= m_fds_alloc || !m_fds[fd].pfn ||
            m_fds[fd].fd_gen != fd_gen) {
            continue;
        }

        event.fd = fd;
        event.revents = m_events[i].events & (EPOLLIN|EPOLLPRI|EPOLLOUT|EPOLLERR|EPOLLHUP);
        event.client = m_fds[fd];
        pthread_mutex_unlock(&m_lock);

        event.client.pfn(&event);

        err = 0;

        pthread_mutex_lock(&m_lock);
    }
    pthread_mutex_unlock(&m_lock);

    return err;
}
