#ifndef RN_HPP
#define RN_HPP

#include <sys/epoll.h>
#include <sys/types.h>
#include <sys/time.h>
#include <assert.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

/* Readiness Notification Library */

struct rn_pollevent_s;
typedef int (*rn_callback_fn_t)(struct rn_pollevent_s *event);

struct rn_client_s {
    /* The callback function. */
    rn_callback_fn_t pfn;
    /* The data to be passed back. */
    void *data;
    /* Increments every time this fd is reopened.
     * Used to make sure no false events get reported. */
    unsigned fd_gen;
};
typedef struct rn_client_s rn_client_t;

struct rn_pollevent_s {
    /* The file descriptor that is ready for I/O. */
    int fd;
    /* The kind of I/O this fd is currently ready for (see 'man epoll_ctl'). */
    short revents;
    /* The object to call to handle I/O on this fd. */
    rn_client_t client;
};
typedef struct rn_pollevent_s rn_pollevent_t;


#define MAKE_CONTEXT(fd, gen)	(((unsigned long long)(gen) << 32) + (fd))
#define CONTEXT_FD(ctx)	((ctx) & ((1ULL << 32) - 1))
#define CONTEXT_GEN(ctx) ((ctx) >> 32)

class RN {
    public:
        RN(int maxfds, int maxevents);
        ~RN();

        /* Prepares the file descriptor/socket for async I/O usage */
        static void PrepareFD(int fd);
        /* Links the file descriptor with the specified callback. When an event 
         * is received for the fd (input/output), the callback will be called */
        int AddIOCallback(int fd, rn_callback_fn_t pfn, void *data);
        /* Removes the fd from RN */
        int RemoveIOCallback(int fd);
        /* Method that waits for an event to happen on the regiestered fds */
        int WaitAndDispatchEvents(int timeout);

        /* Event types that can be received in the I/O callback */
        static const unsigned kInputReady = EPOLLIN;
        static const unsigned kOutputReady = EPOLLOUT;
        static const unsigned kError = EPOLLERR;
        static const unsigned kHangup = EPOLLHUP;

    private:
        int Add(int fd, rn_callback_fn_t pfn, void *data);
        int Remove(int fd);
        bool Exists(int fd);

        int m_epfd;
        rn_client_t *m_fds;
        int m_fds_alloc;
        struct epoll_event *m_events;
        int m_maxevents;
        pthread_mutex_t m_lock;
};

#endif
